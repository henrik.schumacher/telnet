package me.doktormedrasen.backend.telnet;

/**
 * Class by DoktorMedRasen
 */

public abstract class TelnetInstance {

    TelnetListener listener;

    public void setListener(TelnetListener listener) {
        this.listener = listener;
    }

    void onConnect(TelnetHandler handler) {
        if (this.listener != null) {
            this.listener.onConnect(handler);
        }
    }

    void onDisconnect(TelnetHandler handler) {
        if (this.listener != null) {
            this.listener.onDisconnect(handler);
        }
    }

    public static interface TelnetListener {
        public void onConnect(TelnetHandler var1);

        public void onDisconnect(TelnetHandler var1);
    }

}
