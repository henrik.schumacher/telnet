package me.doktormedrasen.backend.telnet;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import java.nio.charset.StandardCharsets;
import java.util.UUID;

/**
 * Class by DoktorMedRasen
 */

public class PacketDataSerializer {

    private ByteBuf buffer;

    PacketDataSerializer(ByteBuf buffer) {
        this.buffer = buffer;
    }

    public PacketDataSerializer() {
        this.buffer = Unpooled.buffer();
    }

    byte[] toByteArray() {
        byte[] bytes = new byte[this.buffer.readableBytes()];
        this.buffer.getBytes(0, bytes);
        return bytes;
    }

    public int lenght() {
        return this.buffer.readableBytes();
    }

    public byte readByte() {
        return this.buffer.readByte();
    }

    public void writeByte(byte b) {
        this.buffer.writeByte((int) b);
    }

    public void readBytes(byte[] bytes) {
        this.buffer.readBytes(bytes);
    }

    public byte[] readBytes(int lenght) {
        byte[] bytes = new byte[lenght];
        this.buffer.readBytes(bytes);
        return bytes;
    }

    public void writeBytes(byte[] bytes) {
        this.buffer.writeBytes(bytes);
    }

    public boolean readBoolean() {
        return this.buffer.readBoolean();
    }

    public void writeBoolean(boolean b) {
        this.buffer.writeBoolean(b);
    }

    public char readChar() {
        return this.buffer.readChar();
    }

    public void writeChar(char c) {
        this.buffer.writeChar((int) c);
    }

    public double readDouble() {
        return this.buffer.readDouble();
    }

    public void writeDouble(double d) {
        this.buffer.writeDouble(d);
    }

    public float readFloat() {
        return this.buffer.readFloat();
    }

    public void writeFloat(float f) {
        this.buffer.writeFloat(f);
    }

    public int readInt() {
        return this.buffer.readInt();
    }

    public void writeInt(int i) {
        this.buffer.writeInt(i);
    }

    public long readLong() {
        return this.buffer.readLong();
    }

    public void writeLong(long l) {
        this.buffer.writeLong(l);
    }

    public int readMedium() {
        return this.buffer.readMedium();
    }

    public void writeMedium(int i) {
        this.buffer.writeMedium(i);
    }

    public short readShort() {
        return this.buffer.readShort();
    }

    public void writeShort(short s) {
        this.buffer.writeShort((int)s);
    }

    public String readString() {
        byte[] bytes = this.readBytes(this.readInt());
        return new String(bytes, StandardCharsets.UTF_8);
    }

    public void writeString(String s) {
        byte[] bytes = s.getBytes(StandardCharsets.UTF_8);
        this.writeInt(bytes.length);
        this.writeBytes(bytes);
    }

    public UUID readUUID() {
        return new UUID(this.readLong(), this.readLong());
    }

    public void writeUUID(UUID uuid) {
        this.writeLong(uuid.getMostSignificantBits());
        this.writeLong(uuid.getLeastSignificantBits());
    }

}
