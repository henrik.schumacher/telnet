package me.doktormedrasen.backend.telnet;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;

import java.util.concurrent.TimeUnit;

/**
 * Class by DoktorMedRasen
 */

public class TelnetClient extends TelnetInstance {

    private String host;
    private int port;
    private boolean keepAlive;
    private EventLoopGroup eventLoopGroup;
    private Channel channel;

    public TelnetClient(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public void setKeepAlive(boolean keepAlive) {
        this.keepAlive = keepAlive;
    }

    public boolean isKeepAlive() {
        return this.keepAlive;
    }

    public ChannelFuture connect() {
        if (this.eventLoopGroup == null) {
            this.eventLoopGroup = TelnetUtils.getEventLoopGroup();
        }
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.group(this.eventLoopGroup);
        bootstrap.channel(TelnetUtils.getChannelClass());
        bootstrap.handler((ChannelHandler) new ChannelInitializer<Channel>(){

            protected void initChannel(Channel channel) throws Exception {
                channel.pipeline().addLast("decoder", (ChannelHandler)new PacketDecoder());
                channel.pipeline().addLast("encoder", (ChannelHandler)new PacketEncoder());
                channel.pipeline().addLast("handler", (ChannelHandler)new TelnetHandler(TelnetClient.this, channel));
            }
        });
        bootstrap.option(ChannelOption.TCP_NODELAY, true);
        bootstrap.option(ChannelOption.SO_KEEPALIVE, true);
        ChannelFuture future = bootstrap.connect(this.host, this.port);
        future.addListener((GenericFutureListener<? extends Future<? super Void>>) ((ChannelFutureListener) channelFuture -> {
            if (!channelFuture.isSuccess() && this.isKeepAlive()) {
                EventLoop loop = channelFuture.channel().eventLoop();
                loop.schedule(this::connect, 1L, TimeUnit.SECONDS);
            }
        }));
        this.channel = future.channel();
        return future;
    }

    public void disconnect() {
        if (this.channel != null) {
            this.channel.close();
        }
        if (this.eventLoopGroup != null) {
            this.eventLoopGroup.shutdownGracefully();
            this.eventLoopGroup = null;
        }
    }

}
