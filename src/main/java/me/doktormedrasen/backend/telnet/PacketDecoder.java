package me.doktormedrasen.backend.telnet;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

/**
 * Class by DoktorMedRasen
 */

public class PacketDecoder extends ByteToMessageDecoder {

    private ByteBuf buffer = Unpooled.buffer();
    private int lenght;

    protected void decode(ChannelHandlerContext channelHandlerContext, ByteBuf byteBuf, List<Object> list) throws Exception {
        PacketDataSerializer packet;
        if (byteBuf.readableBytes() == 0) {
            return;
        }
        while (byteBuf.isReadable()) {
            this.buffer.writeByte((int)byteBuf.readByte());
        }
        while ((packet = this.readPacket()) != null) {
            list.add(packet);
        }
    }

    private PacketDataSerializer readPacket() {
        if (this.lenght == 0 && this.buffer.readableBytes() >= 4) {
            this.lenght = this.buffer.readInt();
        }
        if (this.lenght != 0 && this.buffer.readableBytes() >= this.lenght) {
            ByteBuf packet = Unpooled.buffer();
            byte[] bytes = new byte[this.lenght];
            this.buffer.readBytes(bytes);
            this.buffer.discardReadBytes();
            packet.writeBytes(bytes);
            this.lenght = 0;
            return new PacketDataSerializer(packet);
        }
        return null;
    }

}
