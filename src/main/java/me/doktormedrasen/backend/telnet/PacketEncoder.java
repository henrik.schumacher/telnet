package me.doktormedrasen.backend.telnet;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

/**
 * Class by DoktorMedRasen
 */

public class PacketEncoder extends MessageToByteEncoder<PacketDataSerializer> {

    protected void encode(ChannelHandlerContext channelHandlerContext, PacketDataSerializer packet, ByteBuf byteBuf) throws Exception {
        byte[] bytes = packet.toByteArray();
        byteBuf.writeInt(bytes.length);
        byteBuf.writeBytes(bytes);
    }

}
