package me.doktormedrasen.backend.telnet;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.EventLoop;
import io.netty.channel.SimpleChannelInboundHandler;

import java.net.SocketAddress;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

/**
 * Class by DoktorMedRasen :P
 */

public class TelnetHandler extends SimpleChannelInboundHandler<PacketDataSerializer> {

    private TelnetInstance instance;
    private Channel channel;
    private HashMap<Integer, Class<? extends IncomingPacket>> incoming;
    private HashMap<Class<? extends OutgoingPacket>, Integer> outgoing;

    TelnetHandler(TelnetInstance instance, Channel channel) {
        this.instance = instance;
        this.channel = channel;
        this.incoming = new HashMap();
        this.outgoing = new HashMap();
    }

    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        this.instance.onConnect(this);
    }

    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        TelnetClient client;
        this.instance.onDisconnect(this);
        if (this.instance instanceof TelnetClient && (client = (TelnetClient)this.instance).isKeepAlive()) {
            EventLoop loop = ctx.channel().eventLoop();
            loop.schedule(client::connect, 1L, TimeUnit.SECONDS);
        }
    }

    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
    }

    protected void channelRead0(ChannelHandlerContext channelHandlerContext, PacketDataSerializer packet) throws Exception {
        int id = packet.readInt();
        if (this.incoming.containsKey(id)) {
            IncomingPacket incomingPacket = this.incoming.get(id).newInstance();
            incomingPacket.decode(packet);
            incomingPacket.handle(this);
        }
    }

    public void sendPacket(OutgoingPacket packet) {
        PacketDataSerializer serializer = new PacketDataSerializer();
        for (Class<? extends OutgoingPacket> c : this.outgoing.keySet()) {
            if (!c.equals(packet.getClass())) continue;
            serializer.writeInt(this.outgoing.get(c));
            packet.encode(serializer);
            this.channel.writeAndFlush((Object)serializer);
            return;
        }
    }

    public SocketAddress remoteAddress() {
        return this.channel.remoteAddress();
    }

    public void registerIncomingPacket(int id, Class<? extends IncomingPacket> c) {
        this.incoming.put(id, c);
    }

    public void registerOutgoingPacket(int id, Class<? extends OutgoingPacket> c) {
        this.outgoing.put(c, id);
    }

}