package me.doktormedrasen.backend.telnet;

/**
 * Class by DoktorMedRasen
 */

public interface OutgoingPacket {

    public void encode(PacketDataSerializer var1);

}
