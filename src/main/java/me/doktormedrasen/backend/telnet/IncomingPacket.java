package me.doktormedrasen.backend.telnet;

/**
 * Class by DoktorMedRasen
 */

public interface IncomingPacket {

    public void decode(PacketDataSerializer var1);

    public void handle(TelnetHandler var1);

}
