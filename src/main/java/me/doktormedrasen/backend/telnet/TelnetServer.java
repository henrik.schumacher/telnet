package me.doktormedrasen.backend.telnet;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;

/**
 * Class by DoktorMedRasen
 */

public class TelnetServer extends TelnetInstance {

    private int port;
    private EventLoopGroup bossGroup;
    private EventLoopGroup workerGroup;
    private Channel channel;

    public TelnetServer(int port) {
        this.port = port;
    }

    public ChannelFuture start() {
        this.bossGroup = TelnetUtils.getEventLoopGroup();
        this.workerGroup = TelnetUtils.getEventLoopGroup();
        ServerBootstrap bootstrap = new ServerBootstrap();
        bootstrap.group(this.bossGroup, this.workerGroup);
        bootstrap.channel(TelnetUtils.getServerChannelClass());
        bootstrap.childHandler((ChannelHandler)new ChannelInitializer<Channel>(){

            protected void initChannel(Channel channel) throws Exception {
                channel.pipeline().addLast("decoder", (ChannelHandler)new PacketDecoder());
                channel.pipeline().addLast("encoder", (ChannelHandler)new PacketEncoder());
                channel.pipeline().addLast("handler", (ChannelHandler)new TelnetHandler(TelnetServer.this, channel));
            }
        });
        bootstrap.option(ChannelOption.TCP_NODELAY, true);
        bootstrap.option(ChannelOption.SO_KEEPALIVE, true);
        ChannelFuture future = bootstrap.bind(this.port);
        this.channel = future.channel();
        return future;
    }

    public void stop() {
        if (this.channel != null) {
            this.channel.close();
        }
        if (this.bossGroup != null) {
            this.bossGroup.shutdownGracefully();
        }
        if (this.workerGroup != null) {
            this.workerGroup.shutdownGracefully();
        }
    }

    public int getPort() {
        return this.port;
    }

}
